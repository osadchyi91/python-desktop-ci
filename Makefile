#
# Test targets
#
.PHONY: test-quality;

#
# Test quality
#
test-quality: test-style \
              test-safety;

test-style:
	flake8 python_desktop_ci/

test-safety:
	# Test dependencies for known security vulnerabilities
	sh scripts/safety.sh
