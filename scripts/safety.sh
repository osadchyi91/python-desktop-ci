#!/bin/bash

SAFETY_CHECK=true

for FILE in `find . -name *requirements.txt -type f`; do
    echo "Scanning $FILE..."

    report=$(safety check --full-report -r $FILE)

    if [ $? -ne 0 ]; then
        echo "${report}" | tail -n +15
        echo "\e[31mFailed \e[0m"

        SAFETY_CHECK=false
    else
        echo "\e[32mPassed \e[0m"
    fi

    echo "+"
done

if ! $SAFETY_CHECK; then
    exit 1;
fi
